/** @type {import('tailwindcss').Config} */
export default {
  content: ['./src/**/*.{html,js,svelte,ts}'],
  theme: {
    extend: {
      colors: {
        black: "#2a363b",
        red: "#e84a5f",
        pink: "#ff847c",
        yellow: "#fecea8",
        green: "#99b898",
        cream: "#ebdfd3",
        white: "#faf9f7",
      }
    },
  },
  plugins: [],
}

