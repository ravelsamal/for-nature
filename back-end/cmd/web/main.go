package main

import (
	"log"
	"net/http"
)

func main() {
	mux := http.NewServeMux()

	fs := http.FileServer(http.Dir("./ui/static"))

	mux.HandleFunc("/", index)
	mux.Handle("/static/", http.StripPrefix("/static/", fs))

	s := http.Server{
		Addr:    ":8080",
		Handler: mux,
	}

	log.Println("Listening...")
	s.ListenAndServe()
}
